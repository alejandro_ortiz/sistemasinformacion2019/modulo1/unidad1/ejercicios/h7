﻿-- Ejercicio 1 hoja 7

DROP DATABASE IF EXISTS b20190607;
CREATE DATABASE b20190607;
USE b20190607;

CREATE OR REPLACE TABLE departamentos(
  codDepartamento int AUTO_INCREMENT,
  PRIMARY KEY (codDepartamento)
);

CREATE OR REPLACE TABLE empleados(
  dni varchar(10),
  PRIMARY KEY (dni)  
);

CREATE OR REPLACE TABLE proyectos(
  codProyecto int AUTO_INCREMENT,
  PRIMARY KEY (codProyecto)
);

CREATE OR REPLACE TABLE pertence(
  codDepartamento int,
  dniEmpleado varchar (10),
  PRIMARY KEY (codDepartamento, dniEmpleado),
  UNIQUE KEY (dniEmpleado),
  CONSTRAINT fkPerteneceDepartamento FOREIGN KEY (codDepartamento) REFERENCES departamentos (codDepartamento),
  CONSTRAINT fkPerteneceEmpleado FOREIGN KEY (dniEmpleado) REFERENCES empleados (dni)                         
);

CREATE OR REPLACE TABLE trabaja(
  dniEmpleado varchar(10),
  codProyecto int,
  fecha date,
  PRIMARY KEY (dniEmpleado, codProyecto),
  CONSTRAINT fkTrabajaEmpleado FOREIGN KEY (dniEmpleado) REFERENCES empleados(dni),
  CONSTRAINT fkTrabajaProyecto FOREIGN KEY (codProyecto) REFERENCES proyectos(codProyecto) 
);